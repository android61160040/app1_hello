package com.example.app1midterm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button: Button = findViewById(R.id.button)
        button.setOnClickListener {
            val NameTextView: TextView = findViewById(R.id.name)
            Log.d(TAG, "" + NameTextView.text)
            setContentView(R.layout.hello_ativity)
            val nameTextView: TextView = findViewById(R.id.Name2)
            nameTextView.text = NameTextView.text
        }
    }
}